# flutters

## 第一章 简介

Flutter 多版本运行环境支持工具，支持安装多版本 Flutter，通过 ```--sdk-<version>``` 的方式使用指定 Flutter SDK 目录执行 ```flutter``` 命令。

flutters 是对 flutter 命令的扩展，实现 Flutter 的**多版本**、**多渠道(channel)**、**多运行环境**。

### 第二章 运行环境

系统环境：```macOS```、```Linux```

依赖工具：```git```、```curl```

### 第三章 文件说明

- flutters
    - mflutter
    - mpub

对应的配置文件：
- config/flutters.config

### 第四章 安装说明

1. 拷贝 bin 目录下的所有文件，放入 \~/.bin 目录。
1. 编辑环境变量配置，根据终端配置不同，可能为 \~/.zshrc 或 \~/.bash_profile，更或者其他文件。
1. 将 \~/.bin 目录加入系统环境变量，如：```export PATH=$PATH:~/.bin```。
1. 重启终端，或重新加载环境变量```source ~/.bash_profile```或```source ~/.zshrc```，或其他相应命令。
1. 验证是否安装成功，执行```flutters --sdk-stable --version```，如正常运行则安装成功，如下：

```shell
$ flutters --sdk-stable --version
推荐：
[stable] [beta] [dev] [master] [v1.12.13-hotfixes]

已安装：
[beta] [dev] [jd] [master] [stable] [v1.12.13-hotfixes]

===========================================================================
Args:	[stable] flutter --version
SDK:	[/Users/rengh/workspace/flutter/stable]
Path:	[/Users/rengh]
===========================================================================

Flutter 1.17.5 • channel stable • https://gitee.com/mirrors/Flutter.git
Framework • revision 8af6b2f038 (2 周前) • 2020-06-30 12:53:55 -0700
Engine • revision ee76268252
Tools • Dart 2.8.4
```

### 第五章 使用说明

#### 4.1 flutters

执行该命令任意指令，均会检查指定版本是否存在，不存在时会尝试从 git 仓库安装。

查看 stable 版本：

```shell
$ flutters --version
```

使用 stable 版本创建 flutter_demo 项目：
```shell
$ flutters create -t app --org com.demo.flutter -a java -i objc flutter_demo
```

不指定 ```--sdk-<version>``` 时默认使用 flutters.config 中 SDK 指定的版本，默认 stable，可编辑 config/flutters.config 文件修改默认值。

其他使用方式同官方 flutter 命令。

查看 beta 版本：

```shell
$ flutters --sdk-beta --version
```

查看 dev 版本：

```shell
$ flutters --sdk-dev --version
```

查看 master 版本：

```shell
$ flutters --sdk-master --version
```

#### 4.2 mflutter

查看 stable 版本：

```shell
$ mflutter stable --version
```

使用 stable 版本创建 flutter_demo 项目：
```shell
$ mflutter stable create -t app --org com.demo.flutter -a java -i objc flutter_demo
```

#### 4.3 mpub

查看 stable 版本的帮助信息：

```shell
$ mpub stable -h
```

使用 stable 版本发布：

```shell
$ mpub stable publish
```

### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
